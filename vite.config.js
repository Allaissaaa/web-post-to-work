import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

import { nodeModulesPolyfillPlugin } from 'esbuild-plugins-node-modules-polyfill';
import externals from 'rollup-plugin-node-externals';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      src: "/src",
    },
  },
  optimizeDeps: {
    esbuildOptions: {
      // Enable esbuild polyfill plugins
      plugins: [
        nodeModulesPolyfillPlugin(),
      ],
    },
    // exclude: ['js-big-decimal']
  },
  build: {
    rollupOptions: {
      plugins: [
        externals()
      ],
    },
  },
  define: {
    'process.env': {}
  },
})
