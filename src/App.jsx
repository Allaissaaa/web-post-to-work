import { Routes, Route } from 'react-router-dom';

// Middleware
import PrivateRoute from 'src/pages/components/PrivateRoute';
import GuestRoute from 'src/pages/components/GuestRoute';

// Checkpoint
import Checkpoint from 'src/pages/components/Checkpoint';

// Wrapper
import Wrapper from 'src/pages/components/Wrapper';

// Pages
import NotFound from 'src/pages/not-found';
import Landing from 'src/pages/landing';
import Login from 'src/pages/login';
import Register from 'src/pages/register';
import ResetPassword from 'src/pages/reset-password';
import Dashboard from 'src/pages/dashboard';
import Notifications from 'src/pages/notifications';
import Messages from 'src/pages/messages';
import JobLists from 'src/pages/job-lists';
import JobApplications from 'src/pages/job-applications';
import Companies from 'src/pages/companies';
import JobTypes from 'src/pages/job-types';
import Maintenance from 'src/pages/maintenance';
import Reports from 'src/pages/reports';

import Resume from 'src/pages/resume';
import Identification from 'src/pages/identification';
import CompanyInformation from 'src/pages/company-information';

import EmployeeRankings from 'src/pages/employee-rankings';
import EmployerRankings from 'src/pages/employer-rankings';
import Users from 'src/pages/users';
import Profile from 'src/pages/profile';
import ChangePassword from 'src/pages/change-password';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='*' element={<NotFound />} />
        {/*Authenticated Users*/}
        <Route element={<PrivateRoute />}>
          <Route element={<Checkpoint />}>
            <Route element={<Wrapper />}>
              <Route path="dashboard" element={<Dashboard />} />
              <Route path="notifications" element={<Notifications />} />
              <Route path="messages">
                <Route path=":conversation_id" element={<Messages />} />
                <Route path="" element={<Messages />} />
              </Route>
              <Route path="job-lists" element={<JobLists />} />
              <Route path="job-applications" element={<JobApplications />} />
              <Route path="companies" element={<Companies />} />
              <Route path="job-types" element={<JobTypes />} />
              <Route path="maintenance" element={<Maintenance />} />
              <Route path="reports" element={<Reports />} />
              <Route path="employee-rankings" element={<EmployeeRankings />} />
              <Route path="employer-rankings" element={<EmployerRankings />} />
              <Route path="users">
                <Route path="admins" element={<Users key="admin" role="admin" roleId="1" />} />
                <Route path="clients" element={<Users key="clients" role="client" roleId="2" />} />
                <Route path="freelancers" element={<Users key="freelancers" role="freelancer" roleId="3" />} />
              </Route>
              <Route path="profile" element={<Profile />} />
              <Route path="change-password" element={<ChangePassword />} />
              <Route path="resume" element={<Resume />} />
              <Route path="identification" element={<Identification />} />
              <Route path="company-information" element={<CompanyInformation />} />
            </Route>
          </Route>
        </Route>
        {/*Guest*/}
        <Route element={<GuestRoute />}>
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="reset-password/:token" element={<ResetPassword />} />
        </Route>
        <Route path="/" element={<Landing />} />
      </Routes>
    </div>
  );
}

export default App;
