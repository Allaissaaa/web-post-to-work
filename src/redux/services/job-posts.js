// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { API_URL } from 'src/helpers/config';

// Define a service using a base URL and expected endpoints
export const jobPostApi = createApi({
  reducerPath: 'jobPostApi',
  baseQuery: fetchBaseQuery({ 
    baseUrl: API_URL,
    prepareHeaders: (headers, { getState }) => {
      const token = getState()?.token?.value || '';

      // If we have a token set in state, let's assume that we should be passing it.
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }
      
      headers.set('Accept', 'application/json');

      return headers;
    },
  }),
  tagTypes: ['Job Post'],
  endpoints: (builder) => ({
    getJobPosts: builder.query({
      query: ({ search = '', page = 1, per_page = 10 }) => `/job-posts?search=${search}&page=${page}&per_page=${per_page}`,
      providesTags: ['Job Post'],
    }),
    addJobPost: builder.mutation({
      query: (body) => ({
        url: `/job-posts/create`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Job Post'],
    }),
    deleteJobPost: builder.mutation({
      query: (body) => ({
        url: `/job-posts/delete`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Job Post'],
    }),
    updateJobPost: builder.mutation({
      query: (body) => ({
        url: `/job-posts/update/${body.id}`,
        method: 'POST',
        body: body.data,
      }),
      invalidatesTags: ['Job Post'],
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { 
  useGetJobPostsQuery,
  useAddJobPostMutation,
  useDeleteJobPostMutation,
  useUpdateJobPostMutation,
} = jobPostApi