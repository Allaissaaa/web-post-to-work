// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { API_URL } from 'src/helpers/config';

// Define a service using a base URL and expected endpoints
export const companyApi = createApi({
  reducerPath: 'companyApi',
  baseQuery: fetchBaseQuery({ 
    baseUrl: API_URL,
    prepareHeaders: (headers, { getState }) => {
      const token = getState()?.token?.value || '';

      // If we have a token set in state, let's assume that we should be passing it.
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }
      
      headers.set('Accept', 'application/json');

      return headers;
    },
  }),
  tagTypes: ['Company'],
  endpoints: (builder) => ({
    getCompanies: builder.query({
      query: ({ search = '', page = 1, per_page = 10 }) => `/company-informations?search=${search}&page=${page}&per_page=${per_page}`,
      providesTags: ['Company'],
    }),
    addCompany: builder.mutation({
      query: (body) => ({
        url: `/company-informations/create`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Company'],
    }),
    deleteCompany: builder.mutation({
      query: (body) => ({
        url: `/company-informations/delete`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Company'],
    }),
    updateCompany: builder.mutation({
      query: (body) => ({
        url: `/company-informations/update/${body.id}`,
        method: 'POST',
        body: body.data,
      }),
      invalidatesTags: ['Company'],
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { 
  useGetCompaniesQuery,
  useAddCompanyMutation,
  useDeleteCompanyMutation,
  useUpdateCompanyMutation,
} = companyApi