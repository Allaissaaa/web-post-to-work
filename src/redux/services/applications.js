// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { API_URL } from 'src/helpers/config';

// Define a service using a base URL and expected endpoints
export const applicationApi = createApi({
  reducerPath: 'applicationApi',
  baseQuery: fetchBaseQuery({ 
    baseUrl: API_URL,
    prepareHeaders: (headers, { getState }) => {
      const token = getState()?.token?.value || '';

      // If we have a token set in state, let's assume that we should be passing it.
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }
      
      headers.set('Accept', 'application/json');

      return headers;
    },
  }),
  tagTypes: ['Application'],
  endpoints: (builder) => ({
    getApplications: builder.query({
      query: ({ search = '', page = 1, per_page = 10 }) => `/applications?search=${search}&page=${page}&per_page=${per_page}`,
      providesTags: ['Application'],
    }),
    addApplication: builder.mutation({
      query: (body) => ({
        url: `/applications/create`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Application'],
    }),
    deleteApplication: builder.mutation({
      query: (body) => ({
        url: `/applications/delete`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Application'],
    }),
    updateApplication: builder.mutation({
      query: (body) => ({
        url: `/applications/update/${body.id}`,
        method: 'POST',
        body: body.data,
      }),
      invalidatesTags: ['Application'],
    }),
    approveApplication: builder.mutation({
      query: (body) => ({
        url: '/applications/request/approve',
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Application'],
    }),
    declineApplication: builder.mutation({
      query: (body) => ({
        url: '/applications/request/decline',
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Application'],
    }),
    hiredApplication: builder.mutation({
      query: (body) => ({
        url: '/applications/request/hired',
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Application'],
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { 
  useGetApplicationsQuery,
  useAddApplicationMutation,
  useDeleteApplicationMutation,
  useUpdateApplicationMutation,
  useApproveApplicationMutation,
  useDeclineApplicationMutation,
  useHiredApplicationMutation,
} = applicationApi


