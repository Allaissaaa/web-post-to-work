// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { API_URL } from 'src/helpers/config';

// Define a service using a base URL and expected endpoints
export const jobTypeApi = createApi({
  reducerPath: 'jobTypeApi',
  baseQuery: fetchBaseQuery({ 
    baseUrl: API_URL,
    prepareHeaders: (headers, { getState }) => {
      const token = getState()?.token?.value || '';

      // If we have a token set in state, let's assume that we should be passing it.
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }
      
      headers.set('Accept', 'application/json');

      return headers;
    },
  }),
  tagTypes: ['Job Type'],
  endpoints: (builder) => ({
    getJobTypes: builder.query({
      query: ({ search = '', page = 1, per_page = 10 }) => `/job-types?search=${search}&page=${page}&per_page=${per_page}`,
      providesTags: ['Job Type'],
    }),
    addJobType: builder.mutation({
      query: (body) => ({
        url: `/job-types/create`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Job Type'],
    }),
    deleteJobType: builder.mutation({
      query: (body) => ({
        url: `/job-types/delete`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Job Type'],
    }),
    updateJobType: builder.mutation({
      query: (body) => ({
        url: `/job-types/update/${body.id}`,
        method: 'POST',
        body: body.data,
      }),
      invalidatesTags: ['Job Type'],
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { 
  useGetJobTypesQuery,
  useAddJobTypeMutation,
  useDeleteJobTypeMutation,
  useUpdateJobTypeMutation,
} = jobTypeApi