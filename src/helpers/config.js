export const PROTOCOL = 'https://';
export const PORT = '';
export const BASE_URL = 'posttowork.online';
export const BASE_URL_WITH_PORT = `${PROTOCOL}${BASE_URL}${PORT}`;
export const API_URL = `${BASE_URL_WITH_PORT}/api`;
export const WEB_SOCKET_AUTH = `${API_URL}/broadcasting/auth`;
