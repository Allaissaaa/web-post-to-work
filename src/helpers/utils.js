export const moneyFormat = (number = 0, locale = 'en-PH', currency = 'PHP') => {
  return new Intl.NumberFormat(locale, { style: 'currency', currency }).format(number);
};

export const getPercentage = (number, percent) => {
  return (percent / 100) * number;
};

export const camelCase = string => string.charAt(0).toUpperCase() + string.slice(1);

export const getUserStatus = user => {
  if (user.user_type === 'Individual') {
    return user.identification.status;
  }

  if (user.user_type === 'Company') {
    return user.company_information.status;
  }

  return user.status;
};

export const getVerificationStatus = user => {
  if (user?.user_type === 'Individual') {
      return user?.identification?.status === 'Approved' ? 'Verified' : user?.identification?.status;
  }

  if (user?.user_type === 'Company') {
      return user?.company_information?.status === 'Approved' ? 'Verified' : user?.company_information?.status;
  }

  return null;
};

export const companyOrIndividual = user => {
  if (user?.user_type === 'Individual') {
    return `${user.first_name} ${user.last_name}`;
  }

  if (user?.user_type === 'Company') {
    return user?.company_information?.name;
  }

  return null;
};