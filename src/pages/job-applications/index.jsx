import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card, Table, InputGroup } from 'react-bootstrap';
import { useGetApplicationsQuery } from 'src/redux/services/applications';
import { Message } from './Actions';
import { View } from 'src/pages/users/Actions';
import { FaSync, FaSearch } from 'react-icons/fa';
import { confirm } from '../../shared/confirm';
import Loader from 'src/shared/loader';
import { useAuth } from 'src/hooks/useAuth';
import { useAntMessage } from 'src/context/ant-message';
import { TailSpin } from  'react-loader-spinner';
import Pagination from 'src/shared/pagination';
import { companyOrIndividual } from 'src/helpers/utils';

function Index({ role, roleId }) {
  const auth = useAuth();
  const antMessage = useAntMessage();
  const [page, setPage] = useState(1);
  const [per_page, setPerPage] = useState(10);
  const [search, setSearch] = useState('');
  const { data, error, isLoading, isFetching, refetch } = useGetApplicationsQuery({
    search,
    page,
    per_page,
  });
  
  useEffect(() => {
    refetch();
  }, []);

  useEffect(() => {
    if (search !== '') {
      setPage(1);
    }
  }, [search]);

  console.log(data, error);

  return (
    <>
      <Container>
        <h2 className="mb-3">Job Applications</h2>
        <Card className="rounded-0">
          <Card.Body>
            {error ? (
              <p>Oh no, there was an error</p>
            ) : isLoading ? (
              <Loader />
            ) : data ? (
              <div className="mt-3">
                <div className="d-flex justify-content-end align-items-center mb-3">
                  <div style={{ marginRight: 10, marginLeft: 10 }}>
                    {isFetching ? (
                      <TailSpin
                        height="20"
                        width="20"
                        color="#4fa94d"
                        ariaLabel="tail-spin-loading"
                        radius="1"
                        wrapperStyle={{}}
                        wrapperClass=""
                        visible={true}
                      />
                    ) : <Button variant="link" size="sm" className="text-dark p-0 text-decoration-none" onClick={() => refetch()}><FaSync /> Refresh Table</Button>}
                  </div>
                  <InputGroup style={{ width: 200 }}>
                    <InputGroup.Text>
                      Per Page
                    </InputGroup.Text>
                    <Form.Select
                      onChange={event => setPerPage(event.target.value)}
                    >
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                      <option>100</option>
                    </Form.Select>
                  </InputGroup>
                  <InputGroup style={{ width: 400, marginRight: 10, marginLeft: 10 }}>
                    <Form.Control
                      placeholder="Search..."
                      onChange={event => setSearch(event.target.value)}
                    />
                    <InputGroup.Text>
                      <FaSearch />
                    </InputGroup.Text>
                  </InputGroup>
                </div>
                <Table borderless hover responsive>
                  <thead>
                    <tr>
                      <th>Freelancer</th>
                      <th>Job</th>
                      {(auth.isAdmin || auth.isFrelancer) && <th>Company/Individual</th>}
                      <th>Applied on</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {data.data.map(item => (
                      <tr key={item.id} id={item.id}>
                        <td>{`${item?.user?.first_name} ${item?.user?.last_name}`}</td>
                        <td>{item.job_post.title}</td>
                        {(auth.isAdmin || auth.isFrelancer) && <td>{companyOrIndividual(item.job_post.user)}</td>}
                        <td>{item.created_at_formatted}</td>
                        <td>{item.status}</td>
                        <td>
                          <View application={item} modalTitle={`Job Application / ${item.job_post.title}`} item={item?.user} antMessage={antMessage} role="freelancer" />
                          <Message item={item} />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
                <Pagination
                  page={page}
                  onPageClick={_page => setPage(_page)}
                  data={data}
                  loading={isLoading}
                />
              </div>
            ) : null}
           </Card.Body>
        </Card>
      </Container>
    </>
  );
}

export default Index;