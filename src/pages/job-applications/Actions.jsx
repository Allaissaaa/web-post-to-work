import { useState, useEffect } from 'react';
import { Modal, Form, Button, Row, Col, Table, Image, Dropdown } from 'react-bootstrap';
import { FaEnvelope } from 'react-icons/fa';
import { useAddConversationMutation } from 'src/redux/services/conversations';
import { useNavigate } from 'react-router-dom';

function Message({ item }) {
  const navigate = useNavigate();
  const [addConversation] = useAddConversationMutation();

  return (
    <Button 
      size="sm"
      className="m-1"
      variant="info"
      onClick={async () => {
        const response = await addConversation({ user_id: item.user.id });
     
        navigate(`/messages/${response.data.id}`);
      }}
    >
      <FaEnvelope />
    </Button>
  )
}

export { Message };