import { useState, useEffect, useRef } from 'react';
import { Container, Row, Col, Form, Button, Card, Pagination, Table, Image, Alert } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Loader from 'src/shared/loader';
import { getVerificationStatus } from 'src/helpers/utils';
import { useAuth } from 'src/hooks/useAuth';
import { useDispatch } from 'react-redux';
import { useUploadMutation } from 'src/redux/services/files';
import { useCompanyInformationMutation } from 'src/redux/services/profile';
import { storeUser } from 'src/redux/reducers/user';
import { updateState } from 'src/redux/updateState';
import { useAntMessage } from 'src/context/ant-message';
import { Formik } from 'formik';
import * as Yup from 'yup';

function Index() {
  const navigate = useNavigate();
  const antMessage = useAntMessage();
  const auth = useAuth();
  const dispatch = useDispatch();
  const [fileUploader] = useUploadMutation();
  const [companyInformation] = useCompanyInformationMutation();

  const { name, description, logo, address, email, contact_no, status, reason_to_decline } = auth.getUser.company_information;

  const submit = async (values, callback, setErrors) => {
    try {
      const response = await companyInformation({ ...values, reason_to_decline: null, status: 'Pending' }).unwrap();
      if (response.success) {
        antMessage.success(response.message);
        await dispatch(updateState(storeUser(response.user)));
      } else {
        antMessage.error(response.message);
      }
      callback();
    } catch(error) {
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      callback();
    }
  };

  return (
    <>
      <Container>
        <h2 className="mb-3">Company Information</h2>
        <Card className="rounded-0">
          <Card.Body>
            {(status === 'Pending' || status === 'Approved') ? (
              <>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Logo</dt>
                  <dd className="col-sm-9">
                    <Image src={logo} style={{ width: 200 }} />
                  </dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Name</dt>
                  <dd className="col-sm-9">{name}</dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Description</dt>
                  <dd className="col-sm-9">{description}</dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Address</dt>
                  <dd className="col-sm-9">{address}</dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Email</dt>
                  <dd className="col-sm-9">{email}</dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Contact No.</dt>
                  <dd className="col-sm-9">{contact_no}</dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Status</dt>
                  <dd className="col-sm-9">{getVerificationStatus(auth.getUser)}</dd>
                </dl>
              </>
            ) : (
              <>
                <Alert variant="danger">
                  Please update your company details. Reason to decline: {reason_to_decline}. 
                </Alert>
                <Formik
                  initialValues={{
                    name: '',
                    description: '',
                    logo: '',
                    address: '',
                    email: '',
                    contact_no: '',
                    ...auth.getUser.company_information
                  }}
                  validationSchema={Yup.object().shape({
                    name: Yup.string().required('Name is required').nullable(),
                    description: Yup.string().required('Description is required').nullable(),
                    logo: Yup.string().required('Logo is required').nullable(),
                    address: Yup.string().required('Address is required').nullable(),
                    email: Yup.string().email('Invalid email').required('Email is required'),
                    contact_no: Yup.string().required('Contact no is required').nullable(),
                  })}
                  onSubmit={(values, { setSubmitting, setErrors }) => {
                    setSubmitting(true);
                     setTimeout(() => {
                      submit(values, () => {
                        setSubmitting(false);
                      }, setErrors);
                    }, 400);
                  }}
                >
                 {({
                   values,
                   errors,
                   touched,
                   handleChange,
                   handleBlur,
                   handleSubmit,
                   isSubmitting,
                   setFieldValue
                 }) => (
                  <Form onSubmit={handleSubmit}>
                    {values.logo && <Image key={values.logo} alt={values.logo} src={values.logo} className="mb-4" style={{ width: 200 }} />}
                    <Form.Group className="mb-3">
                      <Form.Label>Logo</Form.Label>
                      <Form.Control 
                        type="file"
                        name="logo"
                        placeholder="Logo"
                        onChange={async (event) => {
                          if (event?.target?.files?.[0]) {
                            try {
                              const data = new FormData();
                              data.append('file', event?.target?.files?.[0]);
                              const response = await fileUploader(data).unwrap();
                              setFieldValue('logo', response.data);
                            } catch(error) {
                              console.log(error);
                            }
                          }
                        }}
                        onBlur={handleBlur}
                        className={errors.logo && touched.logo && 'is-invalid'}
                      />
                      {errors.logo && touched.logo && <div className="invalid-feedback custom-invalid-feedback">{errors.logo}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Name</Form.Label>
                      <Form.Control 
                        type="text"
                        name="name"
                        placeholder="Name"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.name && touched.name && 'is-invalid'}
                      />
                      {errors.name && touched.name && <div className="invalid-feedback custom-invalid-feedback">{errors.name}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Description</Form.Label>
                      <Form.Control
                        as="textarea" 
                        name="description"
                        placeholder="Description"
                        value={values.description}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.description && touched.description && 'is-invalid'}
                      />
                      {errors.description && touched.description && <div className="invalid-feedback custom-invalid-feedback">{errors.description}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Address</Form.Label>
                      <Form.Control
                        as="textarea" 
                        name="address"
                        placeholder="Address"
                        value={values.address}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.address && touched.address && 'is-invalid'}
                      />
                      {errors.address && touched.address && <div className="invalid-feedback custom-invalid-feedback">{errors.address}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Email</Form.Label>
                      <Form.Control 
                        type="email" 
                        name="email"
                        placeholder="Email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.email && touched.email && 'is-invalid'}
                      />
                      {errors.email && touched.email && <div className="invalid-feedback custom-invalid-feedback">{errors.email}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Contact No.</Form.Label>
                      <Form.Control 
                        type="text"
                        name="contact_no"
                        placeholder="Contact No."
                        value={values.contact_no}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.contact_no && touched.contact_no && 'is-invalid'}
                      />
                      {errors.contact_no && touched.contact_no && <div className="invalid-feedback">{errors.contact_no}</div>}
                    </Form.Group>
                    <div className="d-flex justify-content-end align-items-center">
                      <Button 
                        variant="primary"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        {isSubmitting ? 'Please wait...' : 'Submit'}
                      </Button>
                    </div>
                  </Form>
                 )}
                </Formik>
              </>
            )}
          </Card.Body>
        </Card>
      </Container>
    </>
  );
}

export default Index;