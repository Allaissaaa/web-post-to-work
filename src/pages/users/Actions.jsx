import { useState, useEffect } from 'react';
import { Modal, Form, Button, Row, Col, Table, Image, Dropdown, Tab, Tabs } from 'react-bootstrap';
import { FaTrash, FaPencilAlt, FaEye, FaEnvelope, FaFile } from 'react-icons/fa';
import moment from 'moment';
import { useGetUsersQuery, useCreateUserMutation, useUpdateUserMutation, useDeleteUserMutation, useApproveCredentialsMutation, useDeclineCredentialsMutation } from 'src/redux/services/user';
import { useApproveApplicationMutation, useDeclineApplicationMutation, useHiredApplicationMutation } from 'src/redux/services/applications';
import { confirm } from 'src/shared/confirm';
import { getUserStatus } from 'src/helpers/utils';
import { useAntMessage } from 'src/context/ant-message';
import Loader from 'src/shared/loader';
import { Formik } from 'formik';
import * as Yup from 'yup';
import DocViewer, { DocViewerRenderers } from '@cyntler/react-doc-viewer';

const header = { 'admin': 'Admin', 'client': 'Client', 'freelancer': 'Freelancer' };

function Create({ antMessage, role }) {
  const [createUser] = useCreateUserMutation();
  const [show, setShow] = useState(false);

  const submit = async (values, callback, setErrors) => {
    try {
      const response = await createUser({ ...values, role }).unwrap();
      console.log(response);
      if (response.success) {
        setShow(false);
        antMessage.success(response.message);
      } else {
        antMessage.error(response.message);
      }
    } catch(error) {
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      callback();
    }
  };

  const handleClose = () => setShow(false);

  const handleShow = () => setShow(true);

  return (
    <>
      <div className="d-flex justify-content-end">
        <Button onClick={handleShow}>Add {header[role]}</Button>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="border-0" closeButton>
          <Modal.Title>Add {header[role]}</Modal.Title>
        </Modal.Header>
        <Formik
          initialValues={{
            first_name: '',
            last_name: '',
            gender: '',
            date_of_birth: '',
            address: '',
            email: '',
            password: '',
            password_confirmation: '',
          }}
          validationSchema={Yup.object().shape({
            first_name: Yup.string().required('First Name is required').nullable(),
            last_name: Yup.string().required('Last Name is required').nullable(),
            gender: Yup.string().required('Gender is required').nullable(),
            date_of_birth: Yup.string().required('Date of birth is required').nullable(),
            address: Yup.string().required('Address is required').nullable(),
            email: Yup.string().email('Invalid email').required('Required'),
            password: Yup.string().required('Required'),
            password_confirmation: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match')
          })}
          onSubmit={(values, { setSubmitting, setErrors }) => {
            setSubmitting(true);
             setTimeout(() => {
              submit(values, () => {
                setSubmitting(false);
              }, setErrors);
            }, 400);
          }}
        >
         {({
           values,
           errors,
           touched,
           handleChange,
           handleBlur,
           handleSubmit,
           isSubmitting,
         }) => (
          <Form onSubmit={handleSubmit}>
            <Modal.Body>
              <Form.Group className="mb-3">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                  type="text"
                  name="first_name"
                  placeholder="First Name"
                  value={values.first_name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.first_name && touched.first_name && 'is-invalid'}
                />
                {errors.first_name && touched.first_name && <div className="invalid-feedback custom-invalid-feedback">{errors.first_name}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Middle Name</Form.Label>
                <Form.Control 
                  type="text"
                  name="middle_name"
                  placeholder="Middle Name (Optional)"
                  value={values.middle_name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.middle_name && touched.middle_name && 'is-invalid'}
                />
                {errors.middle_name && touched.middle_name && <div className="invalid-feedback">{errors.middle_name}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                  type="text"
                  name="last_name"
                  placeholder="Last Name"
                  value={values.last_name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.last_name && touched.last_name && 'is-invalid'}
                />
                {errors.last_name && touched.last_name && <div className="invalid-feedback custom-invalid-feedback">{errors.last_name}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Gender</Form.Label>
                <Form.Select
                  name="gender"
                  value={values.gender}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.gender && touched.gender && 'is-invalid'}
                >
                  <option value="">Select Gender</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </Form.Select>
                {errors.gender && touched.gender && <div className="invalid-feedback">{errors.gender}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Date of Birth</Form.Label>
                <Form.Control
                  max={moment().format("YYYY-MM-DD")}
                  type="date"
                  name="date_of_birth"
                  placeholder="Date of Birth."
                  value={values.date_of_birth}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.date_of_birth && touched.date_of_birth && 'is-invalid'}
                />
                {errors.date_of_birth && touched.date_of_birth && <div className="invalid-feedback">{errors.date_of_birth}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Address</Form.Label>
                <Form.Control 
                  type="text"
                  name="address"
                  placeholder="Address"
                  value={values.address}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.address && touched.address && 'is-invalid'}
                />
                {errors.address && touched.address && <div className="invalid-feedback custom-invalid-feedback">{errors.address}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Email</Form.Label>
                <Form.Control 
                  type="email" 
                  name="email"
                  placeholder="Email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.email && touched.email && 'is-invalid'}
                />
                {errors.email && touched.email && <div className="invalid-feedback custom-invalid-feedback">{errors.email}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                  type="password" 
                  name="password"
                  placeholder="Password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.password && touched.password && 'is-invalid'}
                />
                {errors.password && touched.password && <div className="invalid-feedback">{errors.password}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Password Confirmation</Form.Label>
                <Form.Control 
                  type="password" 
                  name="password_confirmation"
                  placeholder="Password Confirmation"
                  value={values.password_confirmation}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.password_confirmation && touched.password_confirmation && 'is-invalid'}
                />
                {errors.password_confirmation && touched.password_confirmation && <div className="invalid-feedback">{errors.password_confirmation}</div>}
              </Form.Group>
            </Modal.Body>
            <Modal.Footer className="border-0">
              <Button variant="secondary" onClick={handleClose} >
                Close
              </Button>
              <Button 
                variant="primary"
                type="submit"
                disabled={isSubmitting}
              >
                {isSubmitting ? 'Please wait...' : 'Submit'}
              </Button>
            </Modal.Footer>
          </Form>
         )}
        </Formik>
      </Modal>
    </>
  );
}

function View({ application = null, modalTitle = 'Show Profile', antMessage, role, item }) {
  const [show, setShow] = useState(false);
  const [approveCredential] = useApproveCredentialsMutation();
  const [declineCredential] = useDeclineCredentialsMutation();
  const [approveApplication] = useApproveApplicationMutation();
  const [declineApplication] = useDeclineApplicationMutation();
  const [hiredApplication] = useHiredApplicationMutation();

  const handleClose = () => setShow(false);
  
  const handleShow = () => setShow(true);

  const approve = async () => {
    if (await confirm({ title: 'Approve Credentials?', confirmation: 'Are you sure you want to continue this action?' })) {
      try {
        const response = await approveCredential({ user_id: item.id }).unwrap();
        if (response.success) {
          setShow(false);
          antMessage.success(response.message);
        } else {
          antMessage.error(response.message);
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  const decline = async () => {
    const confirmResponse = await confirm({ 
      title: 'Decline Credentials?', 
      confirmation: 'Are you sure you want to continue this action?',
      inputs: [
        { 
          label: 'Reason to Decline', 
          type: 'text', 
          name: 'reason_to_decline',
        }
      ],
    });
    if (confirmResponse) {
      try {
        const response = await declineCredential({ user_id: item.id, ...confirmResponse }).unwrap();
        if (response.success) {
          setShow(false);
          antMessage.success(response.message);
        } else {
          antMessage.error(response.message);
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  const applicationApproved = async () => {
    if (await confirm({ title: 'Approve Application?', confirmation: 'Are you sure you want to continue this action?' })) {
      try {
        const response = await approveApplication({ id: application.id, user_id: application.user_id }).unwrap();
        if (response.success) {
          setShow(false);
          antMessage.success(response.message);
        } else {
          antMessage.error(response.message);
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  const applicationHired = async () => {
    if (await confirm({ title: 'Hired?', confirmation: 'Are you sure you want to continue this action?' })) {
      try {
        const response = await hiredApplication({ id: application.id, user_id: application.user_id }).unwrap();
        if (response.success) {
          setShow(false);
          antMessage.success(response.message);
        } else {
          antMessage.error(response.message);
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  const applicationDeclined = async () => {
    const confirmResponse = await confirm({ 
      title: 'Decline Application?', 
      confirmation: 'Are you sure you want to continue this action?',
      inputs: [
        { 
          label: 'Reason to Decline', 
          type: 'text', 
          name: 'reason_to_decline',
        }
      ],
    });
    if (confirmResponse) {
      try {
        const response = await declineApplication({ id: application.id, user_id: application.user_id, ...confirmResponse }).unwrap();
        if (response.success) {
          setShow(false);
          antMessage.success(response.message);
        } else {
          antMessage.error(response.message);
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  const personalInformation = () => (
    <Row>
      <Col md={3}>
        <div id="profile-container" className="mb-3 position-relative">
          <Image id="profileImage" src={item.photoURL} className="bg-white border" style={{ objectFit: 'cover' }} alt="" />
        </div>
      </Col>
      <Col md={9}>
        <dl className="row mb-4">
          <dt className="col-sm-3">Name</dt>
          <dd className="col-sm-9">{item.first_name} {item.last_name}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Email</dt>
          <dd className="col-sm-9">{item.email}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Gender</dt>
          <dd className="col-sm-9">{item.gender}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Date of Birth</dt>
          <dd className="col-sm-9">{item.date_of_birth}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Address</dt>
          <dd className="col-sm-9">{item.address}</dd>
        </dl>
      </Col>
    </Row>
  )

  const resume = () => {
    return (
      <DocViewer 
        documents={[{ uri: item?.resume?.resume }]} 
        pluginRenderers={DocViewerRenderers} 
        style={{ width: 500, height: 500 }}
      />
    )
  }

  const identification = () => {
    const { type, number, front_photo, back_photo, status, reason_to_decline } = item.identification;

    return (
      <>
        <dl className="row mb-4">
          <dt className="col-sm-3">Type</dt>
          <dd className="col-sm-9">{type}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Number</dt>
          <dd className="col-sm-9">{number}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Back Photo</dt>
          <dd className="col-sm-9">
            <Image src={back_photo} style={{ width: 200 }} />
          </dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Front Photo</dt>
          <dd className="col-sm-9">
            <Image src={front_photo} style={{ width: 200 }} />
          </dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Status</dt>
          <dd className="col-sm-9">{status}</dd>
        </dl>
        {status === 'Declined' && (
          <dl className="row mb-4">
            <dt className="col-sm-3">Reason to Decline</dt>
            <dd className="col-sm-9">{reason_to_decline}</dd>
          </dl>
        )}
      </>
    )
  }

  const companyInformation = () => {
    const { name, description, logo, address, email, contact_no, status, reason_to_decline  } = item.company_information;

    return (
      <>
        <dl className="row mb-4">
          <dt className="col-sm-3">Logo</dt>
          <dd className="col-sm-9">
            <Image src={logo} style={{ width: 200 }} />
          </dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Name</dt>
          <dd className="col-sm-9">{name}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Description</dt>
          <dd className="col-sm-9">{description}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Address</dt>
          <dd className="col-sm-9">{address}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Email</dt>
          <dd className="col-sm-9">{email}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Contact No.</dt>
          <dd className="col-sm-9">{contact_no}</dd>
        </dl>
        <dl className="row mb-4">
          <dt className="col-sm-3">Status</dt>
          <dd className="col-sm-9">{status}</dd>
        </dl>
        {status === 'Declined' && (
          <dl className="row mb-4">
            <dt className="col-sm-3">Reason to Decline</dt>
            <dd className="col-sm-9">{reason_to_decline}</dd>
          </dl>
        )}
      </>
    )
  }

  return (
    <>
      <Button 
        size="sm"
        className="m-1"
        variant="success"
        onClick={() => setShow(true)}
      >
        <FaEye />
      </Button>
      <Modal size="lg" show={show} onHide={handleClose} backdrop="static" keyboard={false}>
        <Modal.Header className="border-0" closeButton>
          <Modal.Title>{modalTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {role === 'freelancer' && (
          <Tabs
            defaultActiveKey="Personal-Information"
            className="mb-3"
          >
            <Tab eventKey="Personal-Information" title="Personal Information">
              {personalInformation()}
            </Tab>
            <Tab eventKey="Resume" title="Resume">
              {resume()}
            </Tab>
          </Tabs>
          )}
         {role === 'client' && (
          <>
            {item.user_type === 'Individual' && (
              <Tabs
                defaultActiveKey="Personal-Information"
                className="mb-3"
              >
                <Tab eventKey="Personal-Information" title="Personal Information">
                  {personalInformation()}
                </Tab>
                <Tab eventKey="Identification" title="Identification">
                  {identification()}
                </Tab>
              </Tabs>
            )}
            {item.user_type === 'Company' && (
              <Tabs
                defaultActiveKey="Personal-Information"
                className="mb-3"
              >
                <Tab eventKey="Personal-Information" title="Personal Information">
                  {personalInformation()}
                </Tab>
                <Tab eventKey="Company-Information" title="Company Information">
                  {companyInformation()}
                </Tab>
              </Tabs>
            )}
          </>
         )}
        </Modal.Body>
        <Modal.Footer className="border-0">
          {(application && application.status === 'Pending' && role === 'freelancer') && (
            <>
              <Button 
                variant="success"
                onClick={applicationApproved}
              >
                Approve
              </Button>
              <Button 
                variant="danger"
                onClick={applicationDeclined}
              >
                Decline
              </Button>
            </>
          )}
          {(application && application.status === 'Approved' && role === 'freelancer') && (
            <>
              <Button 
                variant="success"
                onClick={applicationHired}
              >
                Hired
              </Button>
            </>
          )}
          {(role === 'client' && getUserStatus(item) === 'Pending') && (
            <>
              <Button 
                variant="success"
                onClick={approve}
              >
                Approve
              </Button>
              <Button 
                variant="danger"
                onClick={decline}
              >
                Decline
              </Button>
            </>
          )}
          <Button variant="secondary" onClick={handleClose} >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

function Edit({ item, antMessage }) {
  const [updateUser] = useUpdateUserMutation();
  const [show, setShow] = useState(false);

  const submit = async (values, callback, setErrors) => {
    try {
      const response = await updateUser({ id: item.id, data: values }).unwrap();
      setShow(false);
      antMessage.success(response.message);
    } catch(error) {
      setShow(false);
      antMessage.error(JSON.stringify(error.data));
    }
  };

  const handleClose = () => setShow(false);

  const handleShow = () => setShow(true);

  return (
    <>
      <Button 
        size="sm"
        className="m-1"
        variant="info"
        onClick={() => setShow(true)}
      >
        <FaPencilAlt />  
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="border-0" closeButton>
          <Modal.Title>Edit {header[item.role]}</Modal.Title>
        </Modal.Header>
        <Formik
          initialValues={{
            first_name: '',
            last_name: '',
            gender: '',
            date_of_birth: '',
            address: '',
            ...item,
          }}
          validationSchema={Yup.object().shape({
            first_name: Yup.string().required('First Name is required').nullable(),
            last_name: Yup.string().required('Last Name is required').nullable(),
            gender: Yup.string().required('Gender is required').nullable(),
            date_of_birth: Yup.string().required('Date of birth is required').nullable(),
            address: Yup.string().required('Address is required').nullable(),
          })}
          onSubmit={(values, { setSubmitting, setErrors }) => {
            setSubmitting(true);
             setTimeout(() => {
              submit(values, () => {
                setSubmitting(false);
              }, setErrors);
            }, 400);
          }}
        >
         {({
           values,
           errors,
           touched,
           handleChange,
           handleBlur,
           handleSubmit,
           isSubmitting,
         }) => (
          <Form onSubmit={handleSubmit}>
            <Modal.Body>
              <Form.Group className="mb-3">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                  type="text"
                  name="first_name"
                  placeholder="First Name"
                  value={values.first_name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.first_name && touched.first_name && 'is-invalid'}
                />
                {errors.first_name && touched.first_name && <div className="invalid-feedback custom-invalid-feedback">{errors.first_name}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Middle Name</Form.Label>
                <Form.Control 
                  type="text"
                  name="middle_name"
                  placeholder="Middle Name (Optional)"
                  value={values.middle_name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.middle_name && touched.middle_name && 'is-invalid'}
                />
                {errors.middle_name && touched.middle_name && <div className="invalid-feedback">{errors.middle_name}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                  type="text"
                  name="last_name"
                  placeholder="Last Name"
                  value={values.last_name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.last_name && touched.last_name && 'is-invalid'}
                />
                {errors.last_name && touched.last_name && <div className="invalid-feedback custom-invalid-feedback">{errors.last_name}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Gender</Form.Label>
                <Form.Select
                  name="gender"
                  value={values.gender}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.gender && touched.gender && 'is-invalid'}
                >
                  <option value="">Select Gender</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </Form.Select>
                {errors.gender && touched.gender && <div className="invalid-feedback">{errors.gender}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Date of Birth</Form.Label>
                <Form.Control
                  max={moment().format("YYYY-MM-DD")}
                  type="date"
                  name="date_of_birth"
                  placeholder="Date of Birth."
                  value={values.date_of_birth}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.date_of_birth && touched.date_of_birth && 'is-invalid'}
                />
                {errors.date_of_birth && touched.date_of_birth && <div className="invalid-feedback">{errors.date_of_birth}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Address</Form.Label>
                <Form.Control 
                  type="text"
                  name="address"
                  placeholder="Address"
                  value={values.address}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.address && touched.address && 'is-invalid'}
                />
                {errors.address && touched.address && <div className="invalid-feedback custom-invalid-feedback">{errors.address}</div>}
              </Form.Group>
            </Modal.Body>
            <Modal.Footer className="border-0">
              <Button variant="secondary" onClick={handleClose} >
                Close
              </Button>
              <Button 
                variant="primary"
                type="submit"
                disabled={isSubmitting}
              >
                {isSubmitting ? 'Please wait...' : 'Submit'}
              </Button>
            </Modal.Footer>
          </Form>
         )}
        </Formik>
      </Modal>
    </>
  );
}

function Delete({ antMessage, item }) {
  const [deleteUser] = useDeleteUserMutation();

  return (
    <Button 
      size="sm"
      className="m-1"
      variant="danger"
      onClick={async () => {
        try {
          if (await confirm({ title: 'Delete Item', confirmation: 'Are you sure you want to delete this item?' })) {
            const response = await deleteUser({ uid: item.uid }).unwrap();
            antMessage.success(response.message);
          }
        } catch (error) {
          antMessage.error(JSON.stringify(error.data));
        }
      }}
    >
      <FaTrash />
    </Button>
  )
}

export { Create, View, Edit, Delete };
