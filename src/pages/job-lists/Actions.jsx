import { useState, useEffect } from 'react';
import { Modal, Form, Button, Row, Col, Table, Image, Dropdown } from 'react-bootstrap';
import { FaTrash, FaPencilAlt, FaEye, FaEnvelope, FaFile } from 'react-icons/fa';
import moment from 'moment';
import { useAddJobPostMutation, useUpdateJobPostMutation, useDeleteJobPostMutation } from 'src/redux/services/job-posts';
import { useGetJobTypesQuery } from 'src/redux/services/job-types';
import { confirm } from 'src/shared/confirm';
import { useAntMessage } from 'src/context/ant-message';
import Loader from 'src/shared/loader';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useAuth } from 'src/hooks/useAuth';

function JobTypesSelect(props) {
  const { data, error, isLoading, isFetching, refetch } = useGetJobTypesQuery({
    search: '',
    page: 1,
    per_page: 1000,
  });

  return (
    <Form.Select
      {...props}
    >
      <option value="">Select Type</option>
       {(data?.data || []).map(item => <option value={item.id}>{item.name}</option>)}
    </Form.Select>
  )
}

function Create({ antMessage }) {
  const auth = useAuth();  
  const [createJobPost] = useAddJobPostMutation();
  const [show, setShow] = useState(false);

  const submit = async (values, callback, setErrors) => {
    try {
      const response = await createJobPost(values).unwrap();
      setShow(false);
      antMessage.success(response.message);
    } catch(error) {
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      callback();
    }
  };

  const handleClose = () => setShow(false);

  const handleShow = () => setShow(true);

  return (
    <>
      <div className="d-flex justify-content-end">
        <Button 
          onClick={() => {
            if (auth.isForApproval()) {
              return antMessage.error('Your identification is currently for approval.');
            }

            if (auth.isDeclined()) {
              return antMessage.error('Your identification is decline please udpate your credentials.');
            }

            return handleShow();
          }}
        >
          Post Job
        </Button>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="border-0" closeButton>
          <Modal.Title>Post Job</Modal.Title>
        </Modal.Header>
        <Formik
          initialValues={{
            job_type_id: '',
            title: '',
            description: '',
          }}
          validationSchema={Yup.object().shape({
            job_type_id: Yup.string().required('Job Type is required').nullable(),
            title: Yup.string().required('Title is required').nullable(),
            description: Yup.string().required('Description is required').nullable(),
          })}
          onSubmit={(values, { setSubmitting, setErrors }) => {
            setSubmitting(true);
             setTimeout(() => {
              submit(values, () => {
                setSubmitting(false);
              }, setErrors);
            }, 400);
          }}
        >
         {({
           values,
           errors,
           touched,
           handleChange,
           handleBlur,
           handleSubmit,
           isSubmitting,
         }) => (
          <Form onSubmit={handleSubmit}>
            <Modal.Body>
              <Form.Group className="mb-3">
                <Form.Label>Type</Form.Label>
                <JobTypesSelect
                  name="job_type_id"
                  value={values.job_type_id}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.job_type_id && touched.job_type_id && 'is-invalid'}
                />
                {errors.job_type_id && touched.job_type_id && <div className="invalid-feedback">{errors.job_type_id}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Title</Form.Label>
                <Form.Control 
                  type="text"
                  name="title"
                  placeholder="Title"
                  value={values.title}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.title && touched.title && 'is-invalid'}
                />
                {errors.title && touched.title && <div className="invalid-feedback">{errors.title}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                  as="textarea"
                  name="description"
                  placeholder="Description"
                  value={values.description}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.description && touched.description && 'is-invalid'}
                />
                {errors.description && touched.description && <div className="invalid-feedback">{errors.description}</div>}
              </Form.Group>
            </Modal.Body>
            <Modal.Footer className="border-0">
              <Button variant="secondary" onClick={handleClose} >
                Close
              </Button>
              <Button 
                variant="primary"
                type="submit"
                disabled={isSubmitting}
              >
                {isSubmitting ? 'Please wait...' : 'Submit'}
              </Button>
            </Modal.Footer>
          </Form>
         )}
        </Formik>
      </Modal>
    </>
  );
}

function View({ item }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  
  const handleShow = () => setShow(true);

  return (
    <>
      <Button 
        size="sm"
        className="m-1"
        variant="success"
        onClick={() => setShow(true)}
      >
        <FaEye />
      </Button>
      <Modal size="lg" show={show} onHide={handleClose} backdrop="static" keyboard={false}>
        <Modal.Header className="border-0" closeButton>
          <Modal.Title>Show Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>

        </Modal.Body>
        <Modal.Footer className="border-0">
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

function Edit({ item, antMessage }) {
  const [updateJobPost] = useUpdateJobPostMutation();
  const [show, setShow] = useState(false);

  const submit = async (values, callback, setErrors) => {
    try {
      const response = await updateJobPost({ id: item.id, data: values }).unwrap();
      setShow(false);
      antMessage.success(response.message);
    } catch(error) {
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      callback();
    }
  };

  const handleClose = () => setShow(false);

  const handleShow = () => setShow(true);

  console.log('item', item);

  return (
    <>
      <Button 
        size="sm"
        className="m-1"
        variant="info"
        onClick={() => setShow(true)}
      >
        <FaPencilAlt />  
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="border-0" closeButton>
          <Modal.Title>Edit Job</Modal.Title>
        </Modal.Header>
        <Formik
          initialValues={{
            job_type_id: '',
            title: '',
            description: '',
            ...item
          }}
          validationSchema={Yup.object().shape({
            job_type_id: Yup.string().required('Job Type is required').nullable(),
            title: Yup.string().required('Title is required').nullable(),
            description: Yup.string().required('Description is required').nullable(),
          })}
          onSubmit={(values, { setSubmitting, setErrors }) => {
            setSubmitting(true);
             setTimeout(() => {
              submit(values, () => {
                setSubmitting(false);
              }, setErrors);
            }, 400);
          }}
        >
         {({
           values,
           errors,
           touched,
           handleChange,
           handleBlur,
           handleSubmit,
           isSubmitting,
         }) => (
          <Form onSubmit={handleSubmit}>
            <Modal.Body>
              <Form.Group className="mb-3">
                <Form.Label>Type</Form.Label>
                <JobTypesSelect
                  name="job_type_id"
                  value={values.job_type_id}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.job_type_id && touched.job_type_id && 'is-invalid'}
                />
                {errors.job_type_id && touched.job_type_id && <div className="invalid-feedback">{errors.job_type_id}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Title</Form.Label>
                <Form.Control 
                  type="text"
                  name="title"
                  placeholder="Title"
                  value={values.title}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.title && touched.title && 'is-invalid'}
                />
                {errors.title && touched.title && <div className="invalid-feedback">{errors.title}</div>}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                  as="textarea"
                  name="description"
                  placeholder="Description"
                  value={values.description}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.description && touched.description && 'is-invalid'}
                />
                {errors.description && touched.description && <div className="invalid-feedback">{errors.description}</div>}
              </Form.Group>
            </Modal.Body>
            <Modal.Footer className="border-0">
              <Button variant="secondary" onClick={handleClose} >
                Close
              </Button>
              <Button 
                variant="primary"
                type="submit"
                disabled={isSubmitting}
              >
                {isSubmitting ? 'Please wait...' : 'Submit'}
              </Button>
            </Modal.Footer>
          </Form>
         )}
        </Formik>
      </Modal>
    </>
  );
}

function Delete({ item, antMessage }) {
  const [deleteJobPost] = useDeleteJobPostMutation();

  return (
    <Button 
      size="sm"
      className="m-1"
      variant="danger"
      onClick={async () => {
        try {
          if (await confirm({ title: 'Delete Item', confirmation: 'Are you sure you want to delete this item?' })) {
            const response = await deleteJobPost({ id: item.id }).unwrap();
            antMessage.success(response.message);
          }
        } catch (error) {
          antMessage.error(JSON.stringify(error.data));
        }
      }}
    >
      <FaTrash />
    </Button>
  )
}

export { Create, View, Edit, Delete };
