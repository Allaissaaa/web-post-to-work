import { useState, useEffect, useRef } from 'react';
import { Container, Row, Col, Form, Button, Card, Alert, Modal, Image } from 'react-bootstrap';
import { Navigate, Outlet } from 'react-router-dom';
import { useAuth } from 'src/hooks/useAuth';
import { useDispatch } from 'react-redux';
import { useUploadMutation } from 'src/redux/services/files';
import { useResumeMutation, useIdentificationMutation, useCompanyInformationMutation } from 'src/redux/services/profile';
import { storeUser } from 'src/redux/reducers/user';
import { updateState } from 'src/redux/updateState';
import { useAntMessage } from 'src/context/ant-message';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { typeOfIds } from 'src/helpers/constant';

// Freelancer
function UploadResume() {
  const antMessage = useAntMessage();
  const auth = useAuth();
  const dispatch = useDispatch();
  const [fileUploader] = useUploadMutation();
  const [resume] = useResumeMutation();
  const input = useRef(null);
  const [file, setFile] = useState(null);
  const [loading, setLoading] = useState(false);

  const submit = async () => {
    setLoading(true);
    try {
      const response = await resume({ resume: file }).unwrap();
      if (response.success) {
        antMessage.success(response.message);
        await dispatch(updateState(storeUser(response.user)));
      } else {
        antMessage.error(response.message);
      }
      setLoading(false);
    } catch(error) {
      console.log(error);
      setLoading(false);
    }
  };

  const onChange = async event => {
    if (event?.target?.files?.[0]) {
      try {
        const data = new FormData();
        data.append('file', event?.target?.files?.[0]);
        const response = await fileUploader(data).unwrap();
        setFile(response.data);
      } catch(error) {
        console.log(error);
      }
    }
  };

  return (
    <>
      <Container className="pt-5 pb-5">
        <Row className="d-flex align-items-center">
          <Col md={{ span: 6, offset: 3 }}>
            <h4>Hi {auth.getName} , Welcome to Post to Work</h4>
            <h5 className="mb-3">Please upload your resume to continue.</h5>
            <Card className="">
              <Card.Body>
                {file && (
                  <>
                    <h6>File: <a href={file} target="_blank">{file}</a></h6>
                  </>
                )}
                <input
                  ref={input}
                  type="file" 
                  onChange={onChange}
                  className="d-none"
                  accept=".pdf"
                />
                <p>Accepted files: PDF.</p>
                <Button
                  size="sm"
                  variant="secondary"
                  className="w-100"
                  onClick={() => input.current.click()}
                  disabled={loading}
                >
                  Upload Resume
                </Button>
                {file && (
                <Button
                  size="sm"
                  variant="primary"
                  className="mt-3 w-100"
                  onClick={submit}
                  disabled={loading}
                >
                  {loading ? 'Please wait...' : 'Submit'}
                </Button>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  )
}

// Client (Individual)
function UploadIndentification() {
  const antMessage = useAntMessage();
  const auth = useAuth();
  const dispatch = useDispatch();
  const [fileUploader] = useUploadMutation();
  const [identification] = useIdentificationMutation();
  const input = useRef(null);
  const [file, setFile] = useState(null);
  const [loading, setLoading] = useState(false);

  const submit = async (values, callback, setErrors) => {
    try {
      const response = await identification(values).unwrap();
      if (response.success) {
        antMessage.success(response.message);
        await dispatch(updateState(storeUser(response.user)));
      } else {
        antMessage.error(response.message);
      }
      callback();
    } catch(error) {
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      callback();
    }
  };

  return (
    <>
      <Container className="pt-5 pb-5">
        <Row className="d-flex align-items-center">
          <Col md={{ span: 6, offset: 3 }}>
            <h4>Hi {auth.getName} , Welcome to Post to Work</h4>
            <h5 className="mb-3">Please upload your identification to continue.</h5>
            <Card className="">
              <Card.Body>
                <Formik
                  initialValues={{
                    type: '',
                    number: '',
                    front_photo: '',
                    back_photo: '',
                  }}
                  validationSchema={Yup.object().shape({
                    type: Yup.string().required('Type is required').nullable(),
                    number: Yup.string().required('Number is required').nullable(),
                    front_photo: Yup.string().required('Front photo is required').nullable(),
                    back_photo: Yup.string().required('Back photo is required').nullable(),
                  })}
                  onSubmit={(values, { setSubmitting, setErrors }) => {
                    setSubmitting(true);
                     setTimeout(() => {
                      submit(values, () => {
                        setSubmitting(false);
                      }, setErrors);
                    }, 400);
                  }}
                >
                 {({
                   values,
                   errors,
                   touched,
                   handleChange,
                   handleBlur,
                   handleSubmit,
                   isSubmitting,
                   setFieldValue
                 }) => (
                  <Form onSubmit={handleSubmit}>
                    <Form.Group className="mb-3">
                      <Form.Label>Type</Form.Label>
                      <Form.Select
                        name="type"
                        value={values.type}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.type && touched.type && 'is-invalid'}
                      >
                        <option value="">Select Type</option>
                        {Object.keys(typeOfIds).map(type => <option key={type} value={type}>{type}</option>)}
                      </Form.Select>
                      {errors.type && touched.type && <div className="invalid-feedback">{errors.type}</div>}
                    </Form.Group>
                    {values.type && (
                      <>
                        <Form.Group className="mb-3">
                          <Form.Label>Number</Form.Label>
                          <Form.Control
                            maxlength={typeOfIds[values.type]?.length} 
                            type="text"
                            name="number"
                            placeholder={typeOfIds[values.type]?.placeholder}
                            value={values.number}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.number && touched.number && 'is-invalid'}
                          />
                          {errors.number && touched.number && <div className="invalid-feedback custom-invalid-feedback">{errors.number}</div>}
                        </Form.Group>
                      </>
                    )}
                    {(values.type && values.number) && (
                    <Row>
                      <Col>
                        {values.front_photo && <Image key={values.front_photo} alt={values.front_photo} src={values.front_photo} className="mb-4 w-100" />}
                        <Form.Group className="mb-3">
                          <Form.Label>Front Photo</Form.Label>
                          <Form.Control 
                            type="file"
                            name="front_photo"
                            placeholder="Front Photo"
                            onChange={async (event) => {
                              if (event?.target?.files?.[0]) {
                                try {
                                  const data = new FormData();
                                  data.append('file', event?.target?.files?.[0]);
                                  const response = await fileUploader(data).unwrap();
                                  setFieldValue('front_photo', response.data);
                                } catch(error) {
                                  console.log(error);
                                }
                              }
                            }}
                            onBlur={handleBlur}
                            className={errors.front_photo && touched.front_photo && 'is-invalid'}
                          />
                          {errors.front_photo && touched.front_photo && <div className="invalid-feedback custom-invalid-feedback">{errors.front_photo}</div>}
                        </Form.Group>
                      </Col>
                      <Col>
                        {values.back_photo && <Image key={values.back_photo} alt={values.back_photo} src={values.back_photo} className="mb-4 w-100" />}
                        <Form.Group className="mb-3">
                          <Form.Label>Back Photo</Form.Label>
                          <Form.Control 
                            type="file"
                            name="back_photo"
                            placeholder="Back Photo"
                            onChange={async (event) => {
                              if (event?.target?.files?.[0]) {
                                try {
                                  const data = new FormData();
                                  data.append('file', event?.target?.files?.[0]);
                                  const response = await fileUploader(data).unwrap();
                                  setFieldValue('back_photo', response.data);
                                } catch(error) {
                                  console.log(error);
                                }
                              }
                            }}
                            onBlur={handleBlur}
                            className={errors.back_photo && touched.back_photo && 'is-invalid'}
                          />
                          {errors.back_photo && touched.back_photo && <div className="invalid-feedback custom-invalid-feedback">{errors.back_photo}</div>}
                        </Form.Group>
                      </Col>
                    </Row>
                    )}
                    <div className="d-flex justify-content-end align-items-center">
                      <Button 
                        variant="primary"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        {isSubmitting ? 'Please wait...' : 'Submit'}
                      </Button>
                    </div>
                  </Form>
                 )}
                </Formik>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  )
}

function CompanyInformation() {
  const antMessage = useAntMessage();
  const auth = useAuth();
  const dispatch = useDispatch();
  const [fileUploader] = useUploadMutation();
  const [companyInformation] = useCompanyInformationMutation();
  const input = useRef(null);
  const [file, setFile] = useState(null);
  const [loading, setLoading] = useState(false);

  const submit = async (values, callback, setErrors) => {
    try {
      const response = await companyInformation(values).unwrap();
      if (response.success) {
        antMessage.success(response.message);
        await dispatch(updateState(storeUser(response.user)));
      } else {
        antMessage.error(response.message);
      }
      callback();
    } catch(error) {
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      callback();
    }
  };

  return (
    <>
      <Container className="pt-5 pb-5">
        <Row className="d-flex align-items-center">
          <Col md={{ span: 6, offset: 3 }}>
            <h4 className="text-center">Hi {auth.getName} , Welcome to Post to Work</h4>
            <h5 className="mb-4 text-center">Please create your company information to continue.</h5>
            <Card className="">
              <Card.Body>
                <Formik
                  initialValues={{
                    name: '',
                    description: '',
                    logo: '',
                    address: '',
                    email: '',
                    contact_no: '',
                  }}
                  validationSchema={Yup.object().shape({
                    name: Yup.string().required('Name is required').nullable(),
                    description: Yup.string().required('Description is required').nullable(),
                    logo: Yup.string().required('Logo is required').nullable(),
                    address: Yup.string().required('Address is required').nullable(),
                    email: Yup.string().email('Invalid email').required('Email is required'),
                    contact_no: Yup.string().required('Contact no is required').nullable(),
                  })}
                  onSubmit={(values, { setSubmitting, setErrors }) => {
                    setSubmitting(true);
                     setTimeout(() => {
                      submit(values, () => {
                        setSubmitting(false);
                      }, setErrors);
                    }, 400);
                  }}
                >
                 {({
                   values,
                   errors,
                   touched,
                   handleChange,
                   handleBlur,
                   handleSubmit,
                   isSubmitting,
                   setFieldValue
                 }) => (
                  <Form onSubmit={handleSubmit}>
                    {values.logo && <Image key={values.logo} alt={values.logo} src={values.logo} className="mb-4" style={{ width: 200 }} />}
                    <Form.Group className="mb-3">
                      <Form.Label>Logo</Form.Label>
                      <Form.Control 
                        type="file"
                        name="logo"
                        placeholder="Logo"
                        onChange={async (event) => {
                          if (event?.target?.files?.[0]) {
                            try {
                              const data = new FormData();
                              data.append('file', event?.target?.files?.[0]);
                              const response = await fileUploader(data).unwrap();
                              setFieldValue('logo', response.data);
                            } catch(error) {
                              console.log(error);
                            }
                          }
                        }}
                        onBlur={handleBlur}
                        className={errors.logo && touched.logo && 'is-invalid'}
                      />
                      {errors.logo && touched.logo && <div className="invalid-feedback custom-invalid-feedback">{errors.logo}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Name</Form.Label>
                      <Form.Control 
                        type="text"
                        name="name"
                        placeholder="Name"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.name && touched.name && 'is-invalid'}
                      />
                      {errors.name && touched.name && <div className="invalid-feedback custom-invalid-feedback">{errors.name}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Description</Form.Label>
                      <Form.Control
                        as="textarea" 
                        name="description"
                        placeholder="Description"
                        value={values.description}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.description && touched.description && 'is-invalid'}
                      />
                      {errors.description && touched.description && <div className="invalid-feedback custom-invalid-feedback">{errors.description}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Address</Form.Label>
                      <Form.Control
                        as="textarea" 
                        name="address"
                        placeholder="Address"
                        value={values.address}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.address && touched.address && 'is-invalid'}
                      />
                      {errors.address && touched.address && <div className="invalid-feedback custom-invalid-feedback">{errors.address}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Email</Form.Label>
                      <Form.Control 
                        type="email" 
                        name="email"
                        placeholder="Email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.email && touched.email && 'is-invalid'}
                      />
                      {errors.email && touched.email && <div className="invalid-feedback custom-invalid-feedback">{errors.email}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Contact No.</Form.Label>
                      <Form.Control 
                        type="text"
                        name="contact_no"
                        placeholder="Contact No."
                        value={values.contact_no}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.contact_no && touched.contact_no && 'is-invalid'}
                      />
                      {errors.contact_no && touched.contact_no && <div className="invalid-feedback">{errors.contact_no}</div>}
                    </Form.Group>
                    <div className="d-flex justify-content-end align-items-center">
                      <Button 
                        variant="primary"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        {isSubmitting ? 'Please wait...' : 'Submit'}
                      </Button>
                    </div>
                  </Form>
                 )}
                </Formik>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  )
}

const Checkpoint = () => {
  const auth = useAuth();

  if (auth.isFrelancer && !auth.getUser.resume) {
    return <UploadResume />;
  }

  if (auth.isClient && auth.isIndividual && !auth.getUser.identification) {
    return <UploadIndentification />
  }

  if (auth.isClient && auth.isCompany && !auth.getUser.company_information) {
    return <CompanyInformation />
  }

  return <Outlet />
};

export default Checkpoint;