import { useState, useEffect } from 'react';
import { Container, Row, Col, Nav, Navbar, NavDropdown, Collapse, Button, Badge } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { FaInbox, FaHome, FaUsers, FaFile, FaUser, FaSignOutAlt, FaLock, FaBell, FaCog, FaPlus, FaMapMarked, FaBuilding, FaList, FaListOl } from 'react-icons/fa';
import { FaMoneyBill, FaMoneyBillTransfer  } from 'react-icons/fa6';
import { FaGears } from 'react-icons/fa6';
import { AiOutlineMenuFold, AiOutlineMenuUnfold } from 'react-icons/ai';
import { RiRadioButtonLine } from 'react-icons/ri';
import logo from 'src/assets/logo.png';
import './Wrapper.scss';
import { confirm } from 'src/shared/confirm';
import Loader from 'src/shared/loader';
import { useAuth } from 'src/hooks/useAuth';
import { getVerificationStatus } from 'src/helpers/utils';
import { useGetNotificationCountQuery } from 'src/redux/services/notifications';
import { useGetUnreadConversationsQuery } from 'src/redux/services/conversations';

function useMessageCount() {
  const { data, error, isLoading, isFetching, refetch } = useGetUnreadConversationsQuery(undefined, { pollingInterval: 3000 });

  console.log(data, error);

  useEffect(() => {
    refetch();
  }, [])

  return data;
}

function useNotificationCount() {
  const { data, error, isLoading, isFetching, refetch } = useGetNotificationCountQuery(undefined, { pollingInterval: 3000 });

  console.log(data, error);

  useEffect(() => {
    refetch();
  }, [])

  return data;
}

function Sidebar() {
  const auth = useAuth();
  const [open, setOpen] = useState(true);
  const message_count = useMessageCount();
  const notification_count = useNotificationCount();

  return (
    <div className="w-100">
      <div className="p-3 text-dark d-flex justify-content-start align-items-center">
        <img src={logo} width={40} /> <span className="ms-2 fw-bold">Post to Work</span>
      </div>
      <Nav className="flex-column">
        <Nav.Link as={Link} className="text-dark fw-bold p-2" to="/dashboard"><FaHome /> <span className="ms-2">Dashboard</span></Nav.Link>
       
        {auth.isAdmin && (
          <>
            <Nav.Link className="text-dark fw-bold p-2 d-flex justify-content-between" onClick={() => setOpen(!open)}><span><FaUsers /> <span className="ms-2">Users</span></span> <FaPlus /></Nav.Link>
            <Collapse in={open}>
              <div className="ps-3">
                <div style={{ borderLeft: '3px solid red' }}>
                  {/*<Nav.Link className="text-dark" as={Link} to="/users/admins">Admins</Nav.Link>*/}
                  <Nav.Link className="text-dark" as={Link} to="/users/clients">Clients</Nav.Link>
                  <Nav.Link className="text-dark" as={Link} to="/users/freelancers">Freelancers</Nav.Link>
                </div>
              </div>
            </Collapse>
          </>
        )}
        <Nav.Link as={Link} className="text-dark fw-bold p-2 d-flex justify-content-between align-items-start" to="/notifications"><span><FaBell /> <span className="ms-2">Notifications</span></span> {notification_count ? <Badge bg="danger">{notification_count}</Badge> : null}</Nav.Link>
        <Nav.Link as={Link} className="text-dark fw-bold p-2 d-flex justify-content-between align-items-start" to="/messages"><span><FaInbox /> <span className="ms-2">Messages</span></span> {message_count ? <Badge bg="danger">{message_count}</Badge> : null}</Nav.Link>
        {auth.isClient && <Nav.Link as={Link} className="text-dark fw-bold p-2" to="/job-lists"><FaList /> <span className="ms-2">Job Lists</span></Nav.Link>}
        {<Nav.Link as={Link} className="text-dark fw-bold p-2" to="/job-applications"><FaFile /> <span className="ms-2">Job Applications</span></Nav.Link>}
        {auth.isAdmin && (
          <>
            <Nav.Link as={Link} className="text-dark fw-bold p-2" to="/job-lists"><FaList /> <span className="ms-2">Job Lists</span></Nav.Link>
            <Nav.Link as={Link} className="text-dark fw-bold p-2" to="/companies"><FaBuilding /> <span className="ms-2">Companies</span></Nav.Link>
            <Nav.Link as={Link} className="text-dark fw-bold p-2" to="/job-types"><FaList /> <span className="ms-2">Job Types</span></Nav.Link>
            <Nav.Link as={Link} className="text-dark fw-bold p-2" to="/employer-rankings"><FaListOl /> <span className="ms-2">Employer Rankings</span></Nav.Link>
            <Nav.Link as={Link} className="text-dark fw-bold p-2" to="/employee-rankings"><FaListOl /> <span className="ms-2">Employee Rankings</span></Nav.Link>
            {/*<Nav.Link as={Link} className="text-dark fw-bold p-2" to="/maintenance"><FaCog /> <span className="ms-2">Maintenance</span></Nav.Link>*/}
            {/*<Nav.Link as={Link} className="text-dark fw-bold p-2" to="/reports"><FaCog /> <span className="ms-2">Reports</span></Nav.Link>*/}
          </>
        )}
        <Nav.Link 
          className="text-dark fw-bold p-2" 
          onClick={async () => {
            if (await confirm({ title: 'Logout', confirmation: 'Are you sure you want to logout?' })) {
              await auth.logout();
            }
          }}
        >
          <FaSignOutAlt /> <span className="ms-2">Logout</span>
         </Nav.Link>
      </Nav>
    </div>
  )
}

function Navigator({ open, setOpen }) {
  const auth = useAuth();

  return (
    <Navbar className="p-3" style={{ backgroundColor: '#002460' }}>
      <Container fluid>
        <Button
          onClick={() => setOpen(!open)}
          variant="link"
          className="text-white h3 p-0"
        >
          <span className="h5">
            {open ? <AiOutlineMenuUnfold /> : <AiOutlineMenuFold />}
          </span>
        </Button>
        <Navbar.Collapse className="justify-content-end">
          <NavDropdown title={<span className="text-white">Signed in as: {`${auth.getName}`} <img src={auth.getAvatar} width="30" height="30" style={{ objectFit: 'cover' }} className="rounded-circle ms-2" /></span>} id="basic-nav-dropdown">
            <NavDropdown.Item as={Link} to="/profile">
              <FaUser /> Update Profile
            </NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/change-password">
              <FaLock /> Change Password
            </NavDropdown.Item>
            {auth.isFrelancer && (
            <NavDropdown.Item as={Link} to="/resume">
              <FaFile /> Resume
            </NavDropdown.Item>
            )}
            {(auth.isClient && auth.isIndividual) && (
            <NavDropdown.Item as={Link} to="/identification">
              <FaFile /> Identification ({getVerificationStatus(auth.getUser)})
            </NavDropdown.Item>
            )}
            {(auth.isClient && auth.isCompany) && (
            <NavDropdown.Item as={Link} to="/company-information">
              <FaFile /> Company Information ({getVerificationStatus(auth.getUser)})
            </NavDropdown.Item>
            )}
            <NavDropdown.Divider />
            <NavDropdown.Item
              onClick={async () => {
                if (await confirm({ title: 'Logout', confirmation: 'Are you sure you want to logout?' })) {
                  await auth.logout();
                }
              }}
            >
              <FaSignOutAlt /> Logout
            </NavDropdown.Item>
          </NavDropdown>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

function Wrapper() {
  const [open, setOpen] = useState(true);

  return (
    <Container fluid>
      <div className="row flex-nowrap">
        {open ? (
          <div className="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-white">
            <div className="d-flex flex-column align-items-center align-items-sm-start pt-2 text-white min-vh-100">
              <Sidebar />
            </div>
          </div>
        ) : null}
        <div className="col p-0">
          <Navigator open={open} setOpen={setOpen} />
          <div className="p-3">
            <Outlet />
          </div>
        </div>
      </div>
    </Container>
  )
}

export default Wrapper;