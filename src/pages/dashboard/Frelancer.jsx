import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card, Table } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Loader from 'src/shared/loader';
import { useGetJobPostsQuery } from 'src/redux/services/job-posts';
import { useAddApplicationMutation } from 'src/redux/services/applications';
import Pagination from 'src/shared/pagination';
import { useAntMessage } from 'src/context/ant-message';
import { companyOrIndividual } from 'src/helpers/utils';

function Index() {
  const antMessage = useAntMessage();
  const navigate = useNavigate();
  const [page, setPage] = useState(1);
  const [per_page, setPerPage] = useState(5);
  const [search, setSearch] = useState('');
  const [createApplication] = useAddApplicationMutation();
  const { data, error, isLoading, isFetching, refetch } = useGetJobPostsQuery({
    search,
    page,
    per_page,
  });

  console.log(data, error);

  useEffect(() => {
    refetch();
  }, []);

  const applyNow = async job_post_id => {
    try {
      const response = await createApplication({ job_post_id }).unwrap();
      console.log(response);
      if (response.success) {
        antMessage.success(response.message);
      } else {
        antMessage.error(response.message);
      }
    } catch(error) {
      console.log(error);
    }
  };

  return (
    <>
      <Container>
        <h2 className="mb-3">Dashboard</h2>
        {error ? (
          <p>Oh no, there was an error</p>
        ) : isLoading ? (
          <Loader />
        ) : data ? (
          <div className="mt-3">
            {data.data.map(item => (
              <div key={item.id} className="mb-3">
                <Card>
                  <Card.Body>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">{item.job_type.name}</Card.Subtitle>
                    <Card.Text>{item.description}</Card.Text>
                    <Card.Text className="mb-0">Posted by: {companyOrIndividual(item.user)}</Card.Text>
                    <Card.Text>Posted on {item.created_at_formatted}</Card.Text>
                    <Button onClick={() => applyNow(item.id)}>Apply Now</Button>
                  </Card.Body>
                </Card>
              </div>
            ))}
            <Pagination
              page={page}
              onPageClick={_page => setPage(_page)}
              data={data}
              loading={isLoading}
            />
          </div>
        ) : null}
      </Container>
    </>
  );
}

export default Index;