import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card, Pagination, Table } from 'react-bootstrap';
import { useGetDashboardQuery } from 'src/redux/services/dashboard';
import { useNavigate } from 'react-router-dom';
import Loader from 'src/shared/loader';
import { Bar } from 'react-chartjs-2';
import { Chart, registerables } from 'chart.js';

Chart.register(...registerables);

function Index() {
  const navigate = useNavigate();
  const { data, error, isLoading, isFetching, refetch } = useGetDashboardQuery({});

  const [chart, setChart] = useState(false);

  useEffect(() => {
    refetch();
    setChart(true);

    return () => setChart(false);
  }, []);

  return (
    <Container>
      <h2 className="mb-3">Dashboard</h2>
      <Row className="mb-4">
        <Col className="d-flex align-items-stretch">
          <Card className="border-0 w-100 bg-primary text-white" as={Button} onClick={() => {}}>
            <Card.Body className="d-flex justify-content-between align-items-center">
              {error ? (
                <p>Oh no, there was an error</p>
              ) : (isLoading || isFetching) ? (
                <Loader />
              ) : data ? (
                <h1 className="p-0 me-3">{data.freelancers}</h1>
              ) : null}
              <p className="mb-0">No of Active Registered Freelancers</p>
            </Card.Body>
          </Card>
        </Col>
        <Col className="d-flex align-items-stretch">
          <Card className="border-0 w-100 bg-primary text-white" as={Button} onClick={() => {}}>
            <Card.Body className="d-flex justify-content-between align-items-center">
              {error ? (
                <p>Oh no, there was an error</p>
              ) : (isLoading || isFetching) ? (
                <Loader />
              ) : data ? (
                <h1 className="p-0 me-3">{data.clients}</h1>
              ) : null}
              <p className="mb-0">No of Active Registered Clients</p>
             </Card.Body>
          </Card>
        </Col>
        <Col className="d-flex align-items-stretch">
          <Card className="border-0 w-100 bg-primary text-white" as={Button} onClick={() => {}}>
            <Card.Body className="d-flex justify-content-between align-items-center">
              {error ? (
                <p>Oh no, there was an error</p>
              ) : (isLoading || isFetching) ? (
                <Loader />
              ) : data ? (
                <h1 className="p-0 me-3">{data.jobs}</h1>
              ) : null}
              <p className="mb-0">Number of Job Posted</p>
             </Card.Body>
          </Card>
        </Col>
        <Col className="d-flex align-items-stretch">
          <Card className="border-0 w-100 bg-primary text-white" as={Button} onClick={() => {}}>
            <Card.Body className="d-flex justify-content-between align-items-center">
              {error ? (
                <p>Oh no, there was an error</p>
              ) : (isLoading || isFetching) ? (
                <Loader />
              ) : data ? (
                <h1 className="p-0 me-3">{data.applications}</h1>
              ) : null}
              <p className="mb-0">Number of Applications</p>
             </Card.Body>
          </Card>
        </Col>
      </Row>
      <Card className="border-0 bg-light-blue mb-5">
        <Card.Body>
          {error ? (
            <>Oh no, there was an error</>
          ) : isLoading ? (
            <><Loader /></>
          ) : data ? (
            chart ? (
              <Bar
                height="400px"
                data={{
                  labels: JSON.parse(data?.chart?.eightMonths),
                  datasets: [
                    {
                      label: 'Clients',
                      data: JSON.parse(data?.chart?.eightMonthsData)['Clients'],
                      backgroundColor: '#333333',
                      borderRadius : 10,
                    },
                    {
                      label: 'Freelancers',
                      data: JSON.parse(data?.chart?.eightMonthsData)['Freelancers'],
                      backgroundColor: '#EEEEEE',
                      borderRadius : 10,
                    },
                  ],
                }}
                options={{
                  maintainAspectRatio: false,
                  scales: {
                    x: {
                      grid: {
                        display: false
                      }
                    },
                    y: {
                      grid: {
                        display: false
                      }
                    }
                  },
                }}
              />
            ) : null
          ) : null}
        </Card.Body>
      </Card>
    </Container>
  );
}

export default Index;