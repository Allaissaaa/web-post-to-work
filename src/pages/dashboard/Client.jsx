import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card, Pagination, Table } from 'react-bootstrap';
import { useGetDashboardQuery } from 'src/redux/services/dashboard';
import { useNavigate } from 'react-router-dom';
import Loader from 'src/shared/loader';

function Index() {
  const navigate = useNavigate();
  const { data, error, isLoading, isFetching, refetch } = useGetDashboardQuery({});

  useEffect(() => {
    refetch();
  }, []);

  return (
    <Container>
      <h2 className="mb-3">Dashboard</h2>
      <Row className="mb-4">
        <Col className="d-flex align-items-stretch">
          <Card className="border-0 w-100 bg-primary text-white" as={Button} onClick={() => {}}>
            <Card.Body className="d-flex justify-content-between align-items-center">
              {error ? (
                <p>Oh no, there was an error</p>
              ) : (isLoading || isFetching) ? (
                <Loader />
              ) : data ? (
                <h1 className="p-0 me-3">{data.jobs}</h1>
              ) : null}
              <p className="mb-0">Number of Job Posted</p>
             </Card.Body>
          </Card>
        </Col>
        <Col className="d-flex align-items-stretch">
          <Card className="border-0 w-100 bg-primary text-white" as={Button} onClick={() => {}}>
            <Card.Body className="d-flex justify-content-between align-items-center">
              {error ? (
                <p>Oh no, there was an error</p>
              ) : (isLoading || isFetching) ? (
                <Loader />
              ) : data ? (
                <h1 className="p-0 me-3">{data.applications}</h1>
              ) : null}
              <p className="mb-0">Number of Applications</p>
             </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Index;