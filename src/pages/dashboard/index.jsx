import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card, Pagination, Table } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Loader from 'src/shared/loader';
import { useAuth } from 'src/hooks/useAuth';
import Admin from './Admin';
import Client from './Client';
import Frelancer from './Frelancer';

function Index() {
  const navigate = useNavigate();
  const auth = useAuth();

  if (auth.isAdmin) return <Admin />;
  if (auth.isClient) return <Client />;
  return <Frelancer />;
}

export default Index;