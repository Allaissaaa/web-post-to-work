import { useState, useEffect, useRef } from 'react';
import { Container, Row, Col, Form, Button, Card, Pagination, Table, Image, Alert } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Loader from 'src/shared/loader';
import { getVerificationStatus } from 'src/helpers/utils';
import { useAuth } from 'src/hooks/useAuth';
import { useDispatch } from 'react-redux';
import { useUploadMutation } from 'src/redux/services/files';
import { useIdentificationMutation } from 'src/redux/services/profile';
import { storeUser } from 'src/redux/reducers/user';
import { updateState } from 'src/redux/updateState';
import { useAntMessage } from 'src/context/ant-message';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { typeOfIds } from 'src/helpers/constant';

function Index() {
  const navigate = useNavigate();
  const antMessage = useAntMessage();
  const auth = useAuth();
  const dispatch = useDispatch();
  const [fileUploader] = useUploadMutation();
  const [identification] = useIdentificationMutation();
  const input = useRef(null);
  const [file, setFile] = useState(null);
  const [loading, setLoading] = useState(false);

  const { type, number, front_photo, back_photo, status, reason_to_decline } = auth.getUser.identification;

  const submit = async (values, callback, setErrors) => {
    try {
      const response = await identification({ ...values, reason_to_decline: null, status: 'Pending' }).unwrap();
      if (response.success) {
        antMessage.success(response.message);
        await dispatch(updateState(storeUser(response.user)));
      } else {
        antMessage.error(response.message);
      }
      callback();
    } catch(error) {
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      callback();
    }
  };

  return (
    <>
      <Container>
        <h2 className="mb-3">Identification</h2>
        <Card className="rounded-0">
          <Card.Body>
            {(status === 'Pending' || status === 'Approved') ? (
              <>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Type</dt>
                  <dd className="col-sm-9">{type}</dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Number</dt>
                  <dd className="col-sm-9">{number}</dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Front Photo</dt>
                  <dd className="col-sm-9">
                    <Image src={front_photo} style={{ width: 200 }} />
                  </dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Back Photo</dt>
                  <dd className="col-sm-9">
                    <Image src={back_photo} style={{ width: 200 }} />
                  </dd>
                </dl>
                <dl className="row mb-4">
                  <dt className="col-sm-3">Status</dt>
                  <dd className="col-sm-9">{getVerificationStatus(auth.getUser)}</dd>
                </dl>
              </>
            ) : (
              <>
                <Alert variant="danger">
                  Please update your company details. Reason to decline: {reason_to_decline}. 
                </Alert>              
                <Formik
                  initialValues={{
                    type: '',
                    number: '',
                    front_photo: '',
                    back_photo: '',
                    ...auth.getUser.identification
                  }}
                  validationSchema={Yup.object().shape({
                    type: Yup.string().required('Type is required').nullable(),
                    number: Yup.string().required('Number is required').nullable(),
                    front_photo: Yup.string().required('Front photo is required').nullable(),
                    back_photo: Yup.string().required('Back photo is required').nullable(),
                  })}
                  onSubmit={(values, { setSubmitting, setErrors }) => {
                    setSubmitting(true);
                     setTimeout(() => {
                      submit(values, () => {
                        setSubmitting(false);
                      }, setErrors);
                    }, 400);
                  }}
                >
                 {({
                   values,
                   errors,
                   touched,
                   handleChange,
                   handleBlur,
                   handleSubmit,
                   isSubmitting,
                   setFieldValue
                 }) => (
                  <Form onSubmit={handleSubmit}>
                    <Form.Group className="mb-3">
                      <Form.Label>Type</Form.Label>
                      <Form.Select
                        name="type"
                        value={values.type}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.type && touched.type && 'is-invalid'}
                      >
                        <option value="">Select Type</option>
                        {Object.keys(typeOfIds).map(type => <option key={type} value={type}>{type}</option>)}
                      </Form.Select>
                      {errors.type && touched.type && <div className="invalid-feedback">{errors.type}</div>}
                    </Form.Group>
                    {values.type && (
                      <>
                        <Form.Group className="mb-3">
                          <Form.Label>Number</Form.Label>
                          <Form.Control
                            maxlength={typeOfIds[values.type]?.length} 
                            type="text"
                            name="number"
                            placeholder={typeOfIds[values.type]?.placeholder}
                            value={values.number}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.number && touched.number && 'is-invalid'}
                          />
                          {errors.number && touched.number && <div className="invalid-feedback custom-invalid-feedback">{errors.number}</div>}
                        </Form.Group>
                      </>
                    )}
                    {(values.type && values.number) && (
                    <Row>
                      <Col>
                        {values.front_photo && <Image key={values.front_photo} alt={values.front_photo} src={values.front_photo} className="mb-4 w-100" />}
                        <Form.Group className="mb-3">
                          <Form.Label>Front Photo</Form.Label>
                          <Form.Control 
                            type="file"
                            name="front_photo"
                            placeholder="Front Photo"
                            onChange={async (event) => {
                              if (event?.target?.files?.[0]) {
                                try {
                                  const data = new FormData();
                                  data.append('file', event?.target?.files?.[0]);
                                  const response = await fileUploader(data).unwrap();
                                  setFieldValue('front_photo', response.data);
                                } catch(error) {
                                  console.log(error);
                                }
                              }
                            }}
                            onBlur={handleBlur}
                            className={errors.front_photo && touched.front_photo && 'is-invalid'}
                          />
                          {errors.front_photo && touched.front_photo && <div className="invalid-feedback custom-invalid-feedback">{errors.front_photo}</div>}
                        </Form.Group>
                      </Col>
                      <Col>
                        {values.back_photo && <Image key={values.back_photo} alt={values.back_photo} src={values.back_photo} className="mb-4 w-100" />}
                        <Form.Group className="mb-3">
                          <Form.Label>Back Photo</Form.Label>
                          <Form.Control 
                            type="file"
                            name="back_photo"
                            placeholder="Back Photo"
                            onChange={async (event) => {
                              if (event?.target?.files?.[0]) {
                                try {
                                  const data = new FormData();
                                  data.append('file', event?.target?.files?.[0]);
                                  const response = await fileUploader(data).unwrap();
                                  setFieldValue('back_photo', response.data);
                                } catch(error) {
                                  console.log(error);
                                }
                              }
                            }}
                            onBlur={handleBlur}
                            className={errors.back_photo && touched.back_photo && 'is-invalid'}
                          />
                          {errors.back_photo && touched.back_photo && <div className="invalid-feedback custom-invalid-feedback">{errors.back_photo}</div>}
                        </Form.Group>
                      </Col>
                    </Row>
                    )}
                    <div className="d-flex justify-content-end align-items-center">
                      <Button 
                        variant="primary"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        {isSubmitting ? 'Please wait...' : 'Submit'}
                      </Button>
                    </div>
                  </Form>
                 )}
                </Formik>
              </>
            )}
          </Card.Body>
        </Card>
      </Container>
    </>
  );
}

export default Index;