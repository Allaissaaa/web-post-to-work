import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card, Alert, Modal } from 'react-bootstrap';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useRegisterMutation, useSendOTPCodeMutation, useVerifyOTPCodeMutation } from 'src/redux/services/register'; 
import { storeUser } from 'src/redux/reducers/user';
import { storeToken } from 'src/redux/reducers/token';
import { updateState } from 'src/redux/updateState';
import { Formik } from 'formik';
import * as Yup from 'yup';
import logo from 'src/assets/logo.png';
import Password from 'src/pages/login/Password';
import moment from 'moment';
import { useAntMessage } from 'src/context/ant-message';

function Index() {
  const antMessage = useAntMessage();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [register] = useRegisterMutation();
  const [verifyOTPCode] = useVerifyOTPCodeMutation();
  const [sendOTPCode] = useSendOTPCodeMutation();
  const [searchParams, setSearchParams] = useSearchParams();
  const [data, setData] = useState({});
  const [token, setToken] = useState({});
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const submitRegister = async (values, callback, setErrors) => {
    try {
      const response = await register(values).unwrap();
      console.log(response);
      if (response.success) {
        setData(values);
        setToken(response.token);
        handleShow();
      } else {
        antMessage.error(response.message);
      }
      callback();
    } catch(error) {
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      callback();
    }
  };

  const resendOTPCode  = async () => {
    try {
      const response = await sendOTPCode(data).unwrap();
      if (response.success) {
        setToken(response.token);
        antMessage.success(response.message);
      } else {
        antMessage.error(response.message);
      }
    } catch(error) {
      antMessage.error(JSON.stringify(error.data));
    }
  };

  const verifyCode  = async (values, callback, setErrors) => {
    try {
      const response = await verifyOTPCode({ token, ...data, ...values }).unwrap();
      console.log(response);
      if (response.success) {
        await dispatch(updateState(storeUser(response.user)));
        await dispatch(updateState(storeToken(response.token)));
        antMessage.success(response.message);
      } else {
        antMessage.error(response.message);
      }
      callback();
    } catch(error) {
      callback();
    }
  };

  return (
    <>
      <Container className="pt-5 pb-5">
        <Row className="d-flex align-items-center">
          <Col md={{ span: 8, offset: 2 }}>
            <div className="p-3 text-center">
              <img src={logo} width={100} className="mb-3" />
              <h4 className="">Post to Work</h4>
            </div>
            <Card className="">
              <Card.Header className="bg-white p-3 border-0" as="h4">
                {{ '2': `Register as Client (${searchParams.get('user_type')})`, '3': 'Register as Freelancer' }[searchParams.get('role_id')]}
              </Card.Header>
              <Card.Body>
                <Formik
                  initialValues={{
                    user_type: searchParams.get('user_type'),
                    role_id: searchParams.get('role_id'),
                    first_name: '',
                    last_name: '',
                    gender: '',
                    date_of_birth: '',
                    address: '',
                    email: '',
                    password: '',
                    password_confirmation: '',
                  }}
                  validationSchema={Yup.object().shape({
                    first_name: Yup.string().required('First Name is required').nullable(),
                    last_name: Yup.string().required('Last Name is required').nullable(),
                    gender: Yup.string().required('Gender is required').nullable(),
                    date_of_birth: Yup.string().required('Date of birth is required').nullable(),
                    address: Yup.string().required('Address is required').nullable(),
                    email: Yup.string().email('Invalid email').required('Email is required'),
                    password: Yup.string().required('Password is required'),
                    password_confirmation: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match')
                  })}
                  onSubmit={(values, { setSubmitting, setErrors }) => {
                    setSubmitting(true);
                    setTimeout(() => {
                      submitRegister(values, () => {
                        setSubmitting(false);
                      }, setErrors);
                    }, 400);
                  }}
                >
                  {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                 }) => (
                  <Form onSubmit={handleSubmit} autoComplete="new-off">
                    <Row>
                      <Col>
                        <Form.Group className="mb-3">
                          <Form.Label>First Name</Form.Label>
                          <Form.Control 
                            type="text"
                            name="first_name"
                            placeholder="First Name"
                            value={values.first_name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.first_name && touched.first_name && 'is-invalid'}
                          />
                          {errors.first_name && touched.first_name && <div className="invalid-feedback custom-invalid-feedback">{errors.first_name}</div>}
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3">
                          <Form.Label>Middle Name</Form.Label>
                          <Form.Control 
                            type="text"
                            name="middle_name"
                            placeholder="Middle Name (Optional)"
                            value={values.middle_name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.middle_name && touched.middle_name && 'is-invalid'}
                          />
                          {errors.middle_name && touched.middle_name && <div className="invalid-feedback">{errors.middle_name}</div>}
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3">
                          <Form.Label>Last Name</Form.Label>
                          <Form.Control 
                            type="text"
                            name="last_name"
                            placeholder="Last Name"
                            value={values.last_name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.last_name && touched.last_name && 'is-invalid'}
                          />
                          {errors.last_name && touched.last_name && <div className="invalid-feedback custom-invalid-feedback">{errors.last_name}</div>}
                        </Form.Group>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Form.Group className="mb-3">
                          <Form.Label>Gender</Form.Label>
                          <Form.Select
                            name="gender"
                            value={values.gender}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.gender && touched.gender && 'is-invalid'}
                          >
                            <option value="">Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                          </Form.Select>
                          {errors.gender && touched.gender && <div className="invalid-feedback">{errors.gender}</div>}
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3">
                          <Form.Label>Date of Birth</Form.Label>
                          <Form.Control
                            max={moment().format("YYYY-MM-DD")}
                            type="date"
                            name="date_of_birth"
                            placeholder="Date of Birth."
                            value={values.date_of_birth}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.date_of_birth && touched.date_of_birth && 'is-invalid'}
                          />
                          {errors.date_of_birth && touched.date_of_birth && <div className="invalid-feedback">{errors.date_of_birth}</div>}
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3">
                          <Form.Label>Address</Form.Label>
                          <Form.Control 
                            type="text"
                            name="address"
                            placeholder="Address"
                            value={values.address}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.address && touched.address && 'is-invalid'}
                          />
                          {errors.address && touched.address && <div className="invalid-feedback custom-invalid-feedback">{errors.address}</div>}
                        </Form.Group>
                      </Col>
                    </Row>
                    <Form.Group className="mb-3">
                      <Form.Label>Email</Form.Label>
                      <Form.Control 
                        type="email" 
                        name="email"
                        placeholder="Email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.email && touched.email && 'is-invalid'}
                      />
                      {errors.email && touched.email && <div className="invalid-feedback custom-invalid-feedback">{errors.email}</div>}
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Password</Form.Label>
                      <Password
                        type="password" 
                        name="password"
                        placeholder="Password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.password && touched.password && 'is-invalid'}
                        renderErrors={() => errors.password && touched.password && <div className="invalid-feedback custom-invalid-feedback" dangerouslySetInnerHTML={{__html: errors.password }}></div>}
                      />
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Password Confirmation</Form.Label>
                      <Password
                        type="password" 
                        name="password_confirmation"
                        placeholder="Password Confirmation"
                        value={values.password_confirmation}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.password_confirmation && touched.password_confirmation && 'is-invalid'}
                        renderErrors={() => errors.password_confirmation && touched.password_confirmation && <div className="invalid-feedback custom-invalid-feedback" dangerouslySetInnerHTML={{__html: errors.password_confirmation }}></div>}
                      />
                    </Form.Group>
                    <Button 
                      disabled={isSubmitting} 
                      variant="primary" 
                      type="submit" 
                      className="w-100 mb-3"
                    >
                      {isSubmitting ? 'Please wait...' : 'Register'}
                    </Button>
                    <div className="d-flex align-items-center justify-content-center">
                      <p className="small mb-0">Already have an account?</p> <Button variant="link" size="sm" onClick={() => navigate('/login')}>Login Here</Button>    
                    </div>
                  </Form>
                 )}
                </Formik>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Verify Email</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Formik
            initialValues={{
              code: '',
            }}
            validationSchema={Yup.object().shape({
              code: Yup.string().required('Code is required').nullable(),
            })}
            onSubmit={(values, { setSubmitting, setErrors }) => {
              setSubmitting(true);
              setTimeout(() => {
                verifyCode(values, () => {
                  setSubmitting(false);
                }, setErrors);
              }, 400);
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
           }) => (
            <Form onSubmit={handleSubmit} autoComplete="new-off">
              <Form.Group className="mb-3">
                <Form.Label>Code</Form.Label>
                <Form.Control 
                  type="text"
                  name="code"
                  placeholder="Code"
                  value={values.code}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.code && touched.code && 'is-invalid'}
                />
                {errors.code && touched.code && <div className="invalid-feedback custom-invalid-feedback">{errors.code}</div>}
              </Form.Group>
              <Button 
                disabled={isSubmitting} 
                variant="primary" 
                type="submit" 
                className="w-100 mb-3"
              >
                {isSubmitting ? 'Please wait...' : 'Submit'}
              </Button>
              <Button 
                disabled={isSubmitting}
                onClick={resendOTPCode}
                variant="secondary" 
                type="button" 
                className="w-100 mb-3"
              >
                Resend Code
              </Button>
            </Form>
           )}
          </Formik>
        </Modal.Body>
      </Modal>
    </>
  )
}

export default Index;