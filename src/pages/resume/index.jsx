import { useState, useEffect, useRef } from 'react';
import { Container, Row, Col, Form, Button, Card, Pagination, Table } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Loader from 'src/shared/loader';
import { useAuth } from 'src/hooks/useAuth';
import { useDispatch } from 'react-redux';
import { useUploadMutation } from 'src/redux/services/files';
import { useResumeMutation } from 'src/redux/services/profile';
import { storeUser } from 'src/redux/reducers/user';
import { updateState } from 'src/redux/updateState';
import { useAntMessage } from 'src/context/ant-message';
import DocViewer, { DocViewerRenderers } from '@cyntler/react-doc-viewer';

function Index() {
  const navigate = useNavigate();
  const antMessage = useAntMessage();
  const auth = useAuth();
  const dispatch = useDispatch();
  const [fileUploader] = useUploadMutation();
  const [resume] = useResumeMutation();
  const input = useRef(null);
  const [loading, setLoading] = useState(false);

  const submit = async file => {
    setLoading(true);
    try {
      const response = await resume({ resume: file }).unwrap();
      console.log(response);
      if (response.success) {
        await dispatch(updateState(storeUser(response.user)));
        antMessage.success(response.message);
      } else {
        antMessage.error(response.message);
      }
      setLoading(false);
    } catch(error) {
      console.log(error);
      setLoading(false);
    }
  };

  const onChange = async event => {
    if (event?.target?.files?.[0]) {
      try {
        const data = new FormData();
        data.append('file', event?.target?.files?.[0]);
        const response = await fileUploader(data).unwrap();
        await submit(response.data);
      } catch(error) {
        console.log(error);
      }
    }
  };

  return (
    <>
      <Container>
        <div className="d-flex justify-content-between align-items-center mb-3">
          <h2 className="mb-0">Resume</h2>
          <div>
            <input
              ref={input}
              type="file" 
              onChange={onChange}
              className="d-none"
              accept=".pdf"
            />
            <Button
              size="sm"
              variant="secondary"
              className="w-100"
              onClick={() => input.current.click()}
              disabled={loading}
            >
              {loading ? 'Please wait...' : 'Update Resume'}
            </Button>
          </div>
        </div>
        <DocViewer 
          key={auth.getUser?.resume?.resume}
          documents={[{ uri: auth.getUser?.resume?.resume }]} 
          pluginRenderers={DocViewerRenderers} 
          style={{ width: 500, height: 500 }}
        />
      </Container>
    </>
  );
}

export default Index;