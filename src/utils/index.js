export const hasError = (field_name = '', errors = []) => {
  const error_object = (errors || []).reduce((obj, cur) => ({...obj, [cur.path]: cur.msg}), {});

  return error_object[field_name] ?? false;
};